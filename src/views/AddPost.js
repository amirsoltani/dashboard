import React,{Component} from 'react';
import {Grid, TextField, FormControlLabel, Switch, Icon, Fab, CardMedia,Button,FormControl} from '@material-ui/core';
import CKEditor from 'ckeditor4-react';
import {ListColumn} from "../components/LictColumn";
import '../styles/css/AddPost.css';
import SettingList from '../components/SettingsList';
import {MultiSelect,SingleSelect} from '../components/Selector';
import {Controler} from '../components/MediaControl';
import axios from 'axios';

export  default class AddPost extends Component{

    content = React.createRef();
    state = {content:"",slider:false,type:1,comment:3,term:[]};
    create(data)
    {
        console.log(data);
            axios.post('http://api.thut.ir/posts/create/',data).
                then(response=>
            {
                alert('true');
            }).catch(error=>
            {
                alert('error');
                console.log(error);
            })
    }
    validation(status)
    {
        const sendData = {obj:{},append:function(name,data){this.obj[name] = data}};
        const  content = this.content.current.editor.getData();
        const {type,comment,tags,genre,star,download,publish,title,thumbnail,country,director,slider,sliderImage} = this.state;
        let meta = [];
        let term = [];
        if(type===1||type===2)
        {
         sendData.append('type',type);
         sendData.append('comment',comment);
         if(title===undefined || title === "")
         {
             return
         }
         sendData.append('title',title);
         if(content==="")
         {
             return
         }
         sendData.append('content',content);
         if(thumbnail === undefined)
         {
             return
         }
         sendData.append('thumbnail',thumbnail.id);
         if(slider)
         {
             sendData.append('slider',slider);
             if(sliderImage===undefined)
             {
                 return
             }
            meta.push({key:'main-slider-photo',value:title,link:sliderImage.id});
         }
         if(publish===undefined || publish ==="")
         {
             return
         }
         meta.push({key:'publish',value:publish});
         if(country===undefined)
         {
             return
         }
         term.push({taxonomy:country});
         if(director===undefined)
         {
             return
         }
         term.push({taxonomy:director});
         if(genre===undefined || genre === [])
         {
             return
         }
         genre.forEach(item=>term.push({taxonomy:item}));
         if(star===undefined || star===[])
         {
             return
         }
         star.forEach(item=>term.push({taxonomy:item}));
         if(tags !== undefined && tags !== [])
         {
             tags.forEach(item=>term.push({taxonomy:item}));
         }
         sendData.append('term',term);
         sendData.append('meta',meta);
         sendData.append('parent','');
         sendData.append('status',status);
         this.create(sendData.obj);
        }

    }
    componentDidMount() {
        axios.get('http://api.thut.ir/detail/')
            .then( (response)=> {
                this.setState({term:response.data})
            })
            .catch( (error)=> {
                // handle error
                console.log(error);
            })
    }

    render() {
        const fab = {marginLeft:7,width:40,height:40};
        const {sliderImage,term} = this.state;
        console.log(this.state);
        return (
            <Grid container spacing={2}>
                <Grid item md={8} >
                    <div className="list-item">
                        <TextField
                            className="title"
                            label="title"
                            id="title"
                            placeholder="Enter title post"
                            onChange={event=>this.setState({title:event.target.value})}
                        />

                    </div>
                    <CKEditor
                        type="classic"
                        id="content"
                        ref={this.content}
                    />
                    <div className="list-item">
                        <ListColumn column={{name:"DownloadLink",column:["Link","Quality"]}}
                                    backData={value=>this.setState({
                                        download:value
                                    })}
                        />
                    </div>
                    <div className="list-item">
                        <MultiSelect name="genre"
                                     backData={value=>this.setState(
                                         {genre:value}
                                     )}
                                     suggestion={term.filter(item=>item.type==='genre')}
                        />
                    </div>
                    <div className="list-item">
                    <MultiSelect name="star" backData={value=>this.setState(
                        {star:value}
                    )}
                                 suggestion={term.filter(item=>item.type==='actress')}
                    />
                    </div>
                    <div className="list-item">
                        <MultiSelect name="tags" backData={value=>this.setState(
                            {tags:value}
                        )}
                                     suggestion={term.filter(item=>item.type==='tag')}
                        />
                    </div>

                </Grid>
                <Grid item md={4} >
                    <SettingList backData={value=>this.setState(
                        {
                            type:value.type,
                            thumbnail:value.image,
                            comment:value.comment,
                        }
                    )} />
                    <div className="list-item">
                    <SingleSelect name="director" backData={value=>this.setState(
                        {director:value}
                    )}
                                  suggestion={term.filter(item=>item.type==='director')}
                    />

                    <br />

                    <SingleSelect name="country" backData={value=>this.setState(
                        {country:value}
                    )}
                                  suggestion={term.filter(item=>item.type==='country')}
                    />

                    </div>
                    <div className="list-item">
                            <TextField
                                       className="year"
                                       label="publish"
                                       id="publish"
                                       placeholder="Enter publish year"
                                       onChange={event=>this.setState(
                                           {publish:event.target.value}
                                       )}
                            />

                            <FormControlLabel
                            control={<Switch color="primary"
                                             onChange={event=>this.setState({slider:event.target.checked})} />}
                            label="Home slider"
                            labelPlacement="start"
                            />
                        {
                            !this.state.slider ?"":
                                <div style={{display:'flex',flexFlow:'warp'}}>
                                <Controler icon={
                                    <Fab color="primary" aria-label="Add" style={fab}>
                                        <Icon style={{fontSize:18}}>add_a_photo</Icon>
                                    </Fab>
                                }
                                           backData={value=>this.setState({sliderImage:value})}
                                />

                                <Fab color="secondary" aria-label="Unset" style={sliderImage?fab:{display:'none'}}
                            onClick={()=>this.setState({sliderImage:undefined})}>
                            <Icon className="fab_icon" style={{fontSize:18}} >block</Icon>
                            </Fab>

                                </div>
                        }
                        {!sliderImage?<div />:
                            <CardMedia
                                className="media"
                                image={sliderImage.image}
                                title="Paella dish"
                            />
                        }
                        <div className="buttong">
                            <Button variant="outlined" color="primary"
                            onClick={()=>this.validation(1)}
                            >Publish</Button>
                            <Button variant="outlined" color="secondary"
                                    onClick={()=>this.validation(2)}
                            >Draft</Button>
                        </div>
                    </div>



                </Grid>
            </Grid>
        )
    }
}

